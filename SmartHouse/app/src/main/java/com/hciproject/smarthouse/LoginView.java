package com.hciproject.smarthouse;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class LoginView extends AppCompatActivity {

    private String TAG = "LoginView";
    private EditText user;
    private EditText password;
    private CheckBox checkBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_view);
        user = (EditText) findViewById(R.id.userEditText);
        password = (EditText) findViewById(R.id.passwordEditText);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        loadPreferences();
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String credentials [] = new String[2];
                credentials[0] = user.getText().toString();
                credentials[1] = password.getText().toString();
                new LoginController(getBaseContext(),LoginView.this, credentials, checkBox.isChecked());
                /*Intent fisico = new Intent(LoginView.this, PhisicalChannel.class);
                startActivity(fisico);*/
            }
        });
    }

    //questa funzione si occupa di verificare i dati inseriti nelle caselle Username e Password
    public void errorCredentials (){
        createDialog("Errore", "Username o password sbagliati");
    }

    private void loadPreferences() {
        LoginController loginController = new LoginController(getApplicationContext());
        checkBox.setChecked(loginController.getIfCredentialsSaved());
        user.setText(loginController.getUser());
        if (loginController.getIfCredentialsSaved()){
            password.setText(loginController.getPassword());
        }
    }

    private void createDialog (String titolo, String messaggio){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titolo).setMessage(messaggio)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}