package com.hciproject.smarthouse;

/**
 * Created by matti on 13/07/2017.
 */

public class DispositiviModel {
    private String nome;
    private String tipologia;
    private String modello;
    private String[] posizioni;
    private String[] listaComandiX;
    private String[] listaComandiY;
    private Object[][] commandPositions;

    public String[] getPosizioni() {
        return posizioni;
    }


    public void setNome(String t) {
        nome = t;
    }

    public void setTipologia(String t) {
        tipologia = t;
    }

    public void setModello(String t) {
        modello = t;
    }

    public void setcomandi(Object[][] commandPosition) {
        this.commandPositions = commandPosition;
    }

    public String getNome() {
        return nome;
    }

    public Object[][] getCommandPositions(){
        return commandPositions;
    }

    public String getModello() {
        return modello;
    }

    public String getTipologia() {
        return tipologia;
    }


}
