package com.hciproject.smarthouse;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by matti on 13/07/2017.
 */

public class JsonController {

    private String TAG = "JsonController";//tag per il log
    //variabile per salvataggio del contesto
    private Context context;
    //nome del Jsonfile da leggere
    private String jSonFile = "template1.json";
    private HomeView homeView;

    //Costruttore per la generazione della lista di dispositivi disponibili
    JsonController(Context ctx, HomeView homeView) {
        this.homeView = homeView;
        context = ctx;
        new GetDispositivi().execute();
    }

    //Questo AsyncTask si occupa di creare il necessario da visualizzae nella view OperaDescView
    private class GetDispositivi extends AsyncTask<Integer, Integer, Object[]> {

        protected Object[] doInBackground(Integer... v) {

            Object [] dispArray = new Object[2]; // lista di oggetti DispositiviModel che verrà restituita alla funzione OnPostExecute
            DispositiviModel tmp; //Oggetto temporaneo che verrà aggiunto di volta in volta a dispArray

            //inizializzazione delle variabili per la copia del contenuto del file Json nella variabile String jsonStr
            String jsonStr;
            InputStream is;
            int size = 0;
            byte[] buffer = null;
            JSONObject jsonObj = null;
            Object ris[] = new Object[4];
            JSONArray arrayComandi;

            try {

                //copia del contenuto del file Json nella variabile String jsonStr
                is = context.getAssets().open(jSonFile);
                size = is.available();
                buffer = new byte[size];
                is.read(buffer);
                is.close();

                jsonStr = new String(buffer, "UTF-8");
                jsonObj = new JSONObject(jsonStr); //assegnazione della stringa ad un oggetto JSONObject

                // prelievo delle informazioni dal json fino a raggiungere l'array delle opere

                JSONArray allDisp = jsonObj.getJSONObject("dispositivi").getJSONArray("fisico");
                List<DispositiviModel> dispPhisical = new ArrayList<DispositiviModel>(allDisp.length());

                // scorrimento di tutto il json fino al raggiungimento dell'indice desiderato
                for (Integer i = 0; i < allDisp.length(); i++) {

                    //tmp e' un oggetto OpereModel temporaneo che verrà poi aggiunto alla lista opereArray
                    tmp = new DispositiviModel();
                    JSONObject c = allDisp.getJSONObject(i);

                    String nome = c.getString("nome"); // nome del dispositivo
                    tmp.setNome(nome);

                    String tipologia = "fisico";
                    tmp.setTipologia(tipologia);

                    String modello = c.getString("modello"); // modello identificativo del dispositivo
                    tmp.setModello(modello);

                    arrayComandi = c.getJSONArray("controlli"); //array per la gestione dei rotori

                    String[] positions = null;


                    /* matrice per la collezione dei vari comandi possibili
                     * commandPosition[n][0] contiene contiene la descrizione del rotore e la sua struttura
                     * commandPosition[n][1] contiene tutte le informazioni sulle posizione che può assumere il rotore
                     */
                    Object[][] commandPosition = new Object[arrayComandi.length()][2];

                    for (int j = 0; j < arrayComandi.length(); j++) {
                        c = arrayComandi.getJSONObject(j);
                        if (c.getString("tipo").equals("knob")){
                            commandPosition[j][0] = c.getString("tipo");
                            String[] commands = new String[4];
                            nome = c.getString("nome"); //nome della manopola
                            commands[0] = nome;
                            String base = c.getString("base"); //immagine di base
                            commands[1] = base;
                            String rotore = c.getString("rotore"); //immagine del rotore
                            commands[2] = rotore;
                            String cliccabile = c.getString("cliccabile"); //se il rotore è cliccabile
                            commands[3] = cliccabile;

                            Object knobs[] = new Object[2];
                            knobs[0] = commands;
                            JSONArray posizioni = c.getJSONArray("posizioni");
                            positions = new String[posizioni.length()];
                            for (Integer k = 0; k < posizioni.length(); k++) {
                                c = posizioni.getJSONObject(k);//i nomi da assegnare alle varie posizioni possibili
                                positions[k] = c.getString(k.toString());
                            }
                            knobs[1] = positions;

                            commandPosition[j][1] = knobs;
                        }else {
                            commandPosition[j][0] = c.getString("tipo");
                            commandPosition[j][1] = c.getString("nome");
                        }
                    }
                    tmp.setcomandi(commandPosition);

                    //aggiunta dell'oggetto tmp alla lista da restituire a fine esecuzione
                    dispPhisical.add(tmp);
                }



                jsonObj = new JSONObject(jsonStr); //assegnazione della stringa ad un oggetto JSONObject

                // prelievo delle informazioni dal json fino a raggiungere l'array dei dispositivi
                allDisp = jsonObj.getJSONObject("dispositivi").getJSONArray("vocale");
                List<DispositiviModel> dispVocal = new ArrayList<DispositiviModel>(allDisp.length());

                // scorrimento di tutto il json fino al raggiungimento dell'indice desiderato
                for (Integer i = 0; i < allDisp.length(); i++) {
                    //tmp e' un oggetto OpereModel temporaneo che verrà poi aggiunto alla lista opereArray
                    tmp = new DispositiviModel();
                    JSONObject c = allDisp.getJSONObject(i);
                    JSONArray comandiVocali = c.getJSONArray("comandi");
                    String[] arrayComandiVocali= new String[comandiVocali.length()];
                    String[] comandiX=new String[comandiVocali.length()];;
                    String[] comandiy=new String[comandiVocali.length()];;
                    String[] comandiz=new String[comandiVocali.length()];;


                    String nome = c.getString("nome"); // nome del dispositivo
                    tmp.setNome(nome);

                    String modello = c.getString("modello"); // modello identificativo del dispositivo
                    tmp.setModello(modello);

                    String tipologia = "vocale";
                    tmp.setTipologia(tipologia);
                    dispVocal.add(tmp);
                }

                dispArray[0] = dispPhisical;
                dispArray[1] = dispVocal;



            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }

            return dispArray;
        }

        @Override
        protected void onPostExecute(Object[] object) {
            //invio dei dati ottenuti al model, con la view a cui fare riferimento
            SingletonDispositiviModel.getInstance().observerAdd(homeView);
            SingletonDispositiviModel.getInstance().setDispList(object);
        }
    }

}
