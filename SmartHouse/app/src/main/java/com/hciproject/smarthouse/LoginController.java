package com.hciproject.smarthouse;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;

/**
 * Created by andre on 12/07/2017.
 */

public class LoginController {
    private Context context;
    private LoginModel loginModel;
    private LoginView loginView;
    private Boolean isChecked;
    LoginController (Context context){
        this.context = context;
        loginModel = new LoginModel(context);
    }

    LoginController (Context context, LoginView loginView, String[] credential, Boolean isChecked){
        this.context = context;
        this.loginView = loginView;
        this.isChecked = isChecked;
        loginModel = new LoginModel(context);
        new CheckCredentials().execute(credential);
    }


    public String getDefaultUser(){
        return loginModel.getDefaultUser();
    }

    public String getUser(){
        return loginModel.getUser();
    }

    public Boolean getIfCredentialsSaved (){
        return loginModel.getIfCredentialsSaved();
    }

    public String getPassword(){
        return loginModel.getPassword();
    }

    public String getDefaultPassword(){
        return loginModel.getDefaultPassword();
    }

    private class CheckCredentials extends AsyncTask<String[],Integer,Boolean>{

        @Override
        protected Boolean doInBackground(String[]... strings) {
            publishProgress();
            try {
                Thread.sleep(1000); //
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //controllo delle stringhe con le informazioni salvate in locale
            if (!strings[0][0].equals(getDefaultUser()) || !strings[0][1].equals(getDefaultPassword())){
                return false; //le stringhe non hanno superato i test
            }
            else {
                loginModel.setUser(strings[0][0]);
                if (isChecked){
                    loginModel.setPassword(strings[0][1]);
                    loginModel.setIfCredentialsSaved(isChecked);
                }
                else{
                    loginModel.setIfCredentialsSaved(isChecked);
                    loginModel.setPassword("");
                }
                return true; //le stringhe hanno superato tutti i test
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            createDialog("Verifica in corso","");
        }

        @Override
        protected void onCancelled() {
            closeDialog();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean) {
                closeDialog();
                new AllViewController(context).toHomeView();
            }
            else {
                closeDialog();
                loginView.errorCredentials();
            }
        }

        private AlertDialog alert;
        private void createDialog (String titolo, String messaggio){
            final AlertDialog.Builder builder = new AlertDialog.Builder(loginView);
            builder.setTitle(titolo).setMessage(messaggio)
                    .setCancelable(false)
                    .setPositiveButton("Annulla", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            CheckCredentials.this.cancel(true);
                        }
                    });
            alert = builder.create();
            alert.show();
        }
        private void closeDialog(){
            alert.hide();
        }
    }
}
