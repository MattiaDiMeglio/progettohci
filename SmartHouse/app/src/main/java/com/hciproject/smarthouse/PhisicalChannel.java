package com.hciproject.smarthouse;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

public class PhisicalChannel extends AppCompatActivity implements Observer {

    private static final String TAG = "PhisicalChannel";
    private static final int REQ_CODE_SPEECH_INPUT = 0;
    private LinearLayout linearLayout;
    private SingletonKnob m_Inst = SingletonKnob.getInstance();
    private TextView textView;
    private List<TextView> comandi = new ArrayList<>();
    private String riassunto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        m_Inst.InitGUIFrame(this);
        setContentView(R.layout.activity_phisical_channel);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                        "Comando vocale");
                startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
            }
        });

        linearLayout = (LinearLayout) findViewById(R.id.mainLinearLayout);
        Button startButton = (Button) findViewById(R.id.startButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDialog("Riepilogo comando", riassunto);
            }
        });
        //new JsonController(getApplicationContext(), this);
        textView = (TextView) findViewById(R.id.textView4);
        //prelievo dell'indice dell'elemento della listview precedentemente scelta
        Integer index = null;
        //ottenimento dell'index dell'opera nella lista
        Bundle extras;
        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if (extras == null) {
                index = null;
            } else {
                index = extras.getInt("index");
                createInterface(SingletonDispositiviModel.getInstance().getAllDisp(), index);
            }
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        Object[] dispList = (Object[]) o;
        //createInterface((List<DispositiviModel>) dispList[0]);
    }

    private void createInterface(List<DispositiviModel> list, Integer index) {
        DispositiviModel dispModel = list.get(index);
        getSupportActionBar().setTitle(dispModel.getNome() + " - " + dispModel.getModello());
        createPhisic(dispModel.getCommandPositions());
    }

    private void createPhisic(Object[][] commandPositions) {
        int numberOfCommand = commandPositions.length;
        int numberOfKnobs = 0;
        int numberOfSlider = 0;
        int j = 0;
        List<Object[]> knobs = new ArrayList<Object[]>();
        List<String> slider = new ArrayList<String>();
        for (int i = 0; i < numberOfCommand; i++) {
            if (commandPositions[i][0].equals("knob")) {
                numberOfKnobs++;
                knobs.add((Object[]) commandPositions[i][1]);
            } else {
                slider.add(commandPositions[i][1].toString());
                numberOfSlider++;
            }
        }
        int tmp = numberOfKnobs;
        if (numberOfKnobs>0){
            for (int i = 0; i < tmp; i++) {
                if (numberOfKnobs < 2) {
                    Object[] o = knobs.get(i);
                    createKnob((String[]) o[0], (String[]) o[1]);
                    numberOfKnobs = numberOfKnobs - 1;
                } else {
                    Object[] o = knobs.get(i);
                    Object[] o1 = knobs.get(i + 1);
                    create2Knobs((String[]) o[0], (String[]) o[1], (String[]) o1[0], (String[]) o1[1]);
                    numberOfKnobs = numberOfKnobs - 2;
                    i++;
                }
            }
        }
        if (numberOfSlider>0){
            createSlider(slider, numberOfSlider);
        }
        updateDisplay();
    }

    private void createSlider(final List<String> slider, int numberOfSlider) {
        for (int i = 0; i < numberOfSlider; i++) {
            LinearLayout linearHorizontal = new LinearLayout(this);
            linearHorizontal.setOrientation(LinearLayout.HORIZONTAL);
            linearHorizontal.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            linearHorizontal.setGravity(Gravity.CENTER_HORIZONTAL);
            linearLayout.addView(linearHorizontal);

            LinearLayout linear = new LinearLayout(this);
            linear.setOrientation(LinearLayout.VERTICAL);
            linear.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            linear.setGravity(Gravity.CENTER);
            linearHorizontal.addView(linear);

            final TextView textView = new TextView(getApplicationContext());
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
            final String descrizione = slider.get(i);
            textView.setText(descrizione);
            comandi.add(textView);

            SeekBar seekbar = new SeekBar(getApplicationContext());
            seekbar.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            linear.addView(seekbar);
            linear.addView(textView);

            seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    textView.setText(descrizione + " - " + i);
                    updateDisplay();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }

            });
        }
    }

    private void createKnob(final String[] desc, final String[] knob) {
        LinearLayout linearHorizontal = new LinearLayout(this);
        linearHorizontal.setOrientation(LinearLayout.HORIZONTAL);
        linearHorizontal.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linearHorizontal.setGravity(Gravity.CENTER_HORIZONTAL);
        linearLayout.addView(linearHorizontal);

        LinearLayout linear = new LinearLayout(this);
        linear.setOrientation(LinearLayout.VERTICAL);
        linear.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linear.setGravity(Gravity.CENTER_HORIZONTAL);
        linearHorizontal.addView(linear);

        final TextView textView = new TextView(getApplicationContext());
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        textView.setText(desc[0]);
        comandi.add(textView);


        RoundKnobButton rv = new RoundKnobButton(this, getResources().getIdentifier(desc[1], "drawable", getPackageName()), getResources().getIdentifier(desc[2], "drawable", getPackageName()), R.drawable.rotoroff, m_Inst.Scale(250), m_Inst.Scale(250), knob.length);

        linear.addView(rv);
        linear.addView(textView);

        rv.setRotorPercentage(0);
        rv.SetListener(new RoundKnobButton.RoundKnobButtonListener() {
            public void onStateChange(boolean newstate) {

            }

            public void onRotate(final int percentage) {
                if (percentage == 99) {
                    textView.setText(desc[0] + " - " + knob[knob.length - 1]);
                } else {
                    textView.setText(desc[0] + " - " + knob[percentage * knob.length / 100]);
                }
                updateDisplay();
            }
        });

    }

    private void create2Knobs(final String[] desc, final String[] knob, final String[] desc1, final String[] knob1) {
        LinearLayout linearHorizontal = new LinearLayout(this);
        linearHorizontal.setOrientation(LinearLayout.HORIZONTAL);
        linearHorizontal.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linearHorizontal.setGravity(Gravity.CENTER_HORIZONTAL);
        linearLayout.addView(linearHorizontal);

        LinearLayout linear = new LinearLayout(this);
        linear.setOrientation(LinearLayout.VERTICAL);
        linear.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linear.setGravity(Gravity.CENTER_HORIZONTAL);
        linearHorizontal.addView(linear);

        final TextView textView = new TextView(getApplicationContext());
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        textView.setText(desc[0] + " - " + knob[0]);
        comandi.add(textView);

        RoundKnobButton rv = new RoundKnobButton(this, getResources().getIdentifier(desc[1], "drawable", getPackageName()), getResources().getIdentifier(desc[2], "drawable", getPackageName()), R.drawable.rotoroff, m_Inst.Scale(250), m_Inst.Scale(250), knob.length);

        linear.addView(rv);
        linear.addView(textView);

        rv.setRotorPercentage(0);
        rv.SetListener(new RoundKnobButton.RoundKnobButtonListener() {
            public void onStateChange(boolean newstate) {

            }

            public void onRotate(final int percentage) {
                if (percentage == 99) {
                    textView.setText(desc[0] + " - " + knob[knob.length - 1]);
                } else {
                    textView.setText(desc[0] + " - " + knob[percentage * knob.length / 100]);
                }
                updateDisplay();
            }
        });

        linear = new LinearLayout(this);
        linear.setOrientation(LinearLayout.VERTICAL);
        linear.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linear.setGravity(Gravity.CENTER_HORIZONTAL);
        linearHorizontal.addView(linear);

        RoundKnobButton rv1 = new RoundKnobButton(this, R.drawable.stator, R.drawable.rotoron, R.drawable.rotoroff, m_Inst.Scale(250), m_Inst.Scale(250), knob.length);
        final TextView textView2 = new TextView(getApplicationContext());
        textView2.setGravity(Gravity.CENTER_HORIZONTAL);
        textView2.setText(desc1[0] + " - " + knob1[0]);
        comandi.add(textView2);


        linear.addView(rv1);
        linear.addView(textView2);

        rv1.setRotorPercentage(0);
        rv1.SetListener(new RoundKnobButton.RoundKnobButtonListener() {
            public void onStateChange(boolean newstate) {

            }

            public void onRotate(final int percentage) {
                if (percentage == 99) {
                    textView2.setText(desc1[0] + " - " + knob1[knob1.length - 1]);
                } else {
                    textView2.setText(desc1[0] + " - " + knob1[percentage * knob1.length / 100]);
                }
                updateDisplay();
            }
        });
    }

    private void updateDisplay() {
        riassunto="| ";
        for (int i =0;i<comandi.size();i++){
            riassunto = riassunto + comandi.get(i).getText().toString() + " | ";
        }
        textView.setText(riassunto);
    }

    private AlertDialog alert;
    private void createDialog (String t, String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View addView = inflater.inflate(R.layout.dialog_avatar, null);
        builder.setTitle(t)
                .setView(addView)
                .setMessage(m)
                .setCancelable(true)
                .setPositiveButton("Conferma", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        closeDialog();
                        createPositiveDialog("Comando Inviato", "");
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private void createPositiveDialog(String t, String m) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View addView = inflater.inflate(R.layout.dialog_avatar, null);
        builder.setTitle("")
                .setView(addView)
                .setMessage(t)
                .setCancelable(true)
                .setPositiveButton("ok", null);
        alert = builder.create();
        alert.show();
    }

    private void closeDialog(){
        alert.hide();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                     createDialog("Comando vocale", result.get(0));
                }
                break;
            }
        }
    }
}