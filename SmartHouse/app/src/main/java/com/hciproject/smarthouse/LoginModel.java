package com.hciproject.smarthouse;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by andre on 12/07/2017.
 */

public class LoginModel{

    private String TAG = "SettingsModel";
    private SharedPreferences sharedPreferences;
    private static final String PREFS_NAME = "MyPrefsFile";

    LoginModel(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
    }

    public void setUser (String user){
        String prefUser = "prefUser";
        sharedPreferences.edit().putString(prefUser, user).apply();
    }
    public String getUser (){
        String prefUser = "prefUser";
        return sharedPreferences.getString(prefUser, "");
    }
    public String getDefaultUser(){
        return "user";
    }
    public String getDefaultPassword(){
        return "password";
    }

    public void setIfCredentialsSaved(boolean save){
        String prefCredentials = "prefCredentials";
        sharedPreferences.edit().putBoolean(prefCredentials, save).apply();
    }

    public Boolean getIfCredentialsSaved(){
        String prefCredentials = "prefCredentials";
        return sharedPreferences.getBoolean(prefCredentials, false);
    }

    public void setPassword(String string) {
        String prefPassword = "prefPassword";
        sharedPreferences.edit().putString(prefPassword, string).apply();
    }

    public String getPassword() {
        String prefPassword = "prefPassword";
        return sharedPreferences.getString(prefPassword, "");
    }
}
