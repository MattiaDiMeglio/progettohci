package com.hciproject.smarthouse;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import java.util.List;

/**
 * Created by matti on 13/07/2017.
 */

public class VocalController {
    private HomeView homeview;
    private SingletonDispositiviModel singleton;
    private Context context;

    public VocalController(Context context, String result, HomeView homeView) {
        this.homeview = homeView;
        this.context = context;
        switch (result.toLowerCase()) {
            case "cosa c'è in frigo":
            case "qual è il contenuto del frigorifero":
            case "cosa c'è in frigorifero":
                createPositiveDialog("Ecco la lista del contenuto del frigo", "6 uova, 2 litri di latte e 2 etti di formaggio brie");
                break;
            case "metti la temperatura del frigorifero a 6 gradi":
            case "cambia la temperatura del frigorifero a 6 gradi":
            case "metti la temperatura del frigo a 6 gradi":
            case "cambia la temperatura del frigo a 6 gradi":
                createDialog("Conferma invio del comando", "Vuoi impostare la temperatura a 6°?");
                break;
            case "fammi un caffé domani alle 7":
                createDialog("Conferma invio comando", "Vuoi un caffé domani mattina alle 7?");
                break;
            default:
                createErrorDialog("Non ho capito bene", "puoi ripetere per favore?");
                break;
        }
        /*String[] tokens = getToken(result);
        String[] nomeDispositivi = getDispositivi();
        for (int i=0; i<nomeDispositivi.length; i++){
            if(nomeDispositivi[i].equals(tokens[0])){
                //toDevice(tokens, nomeDispositivi[i]);
            }
        }*/

    }

    public VocalController(Context context, String nomeDipositivo, String result) {
        String[] tokens = getToken(result);
        //toDevice(tokens, nomeDipositivo);
    }



    public String[] getToken (String command){
        String[] tokens = command.split(" ");
        return tokens;
    }



    private String[] getDispositivi(){
        //todo ho modificato i get nel singleton. Se ti servono solo i dispositivi voccali fai getDispPhisical
        //se servono i vocali fai getDispVocal, se ti servono tutti fai getAllDisp. Sono già del tipo List<DispositiviModel>
        singleton = SingletonDispositiviModel.getInstance();
        List<DispositiviModel> dispList = singleton.getDispVocal();
        String [] nomeDispotivi= new String[dispList.size()];

        for (int i=0; i<dispList.size(); i++){
            nomeDispotivi[i]= dispList.get(i).getNome();
        }
        return nomeDispotivi;
    }

    private AlertDialog alert;
    private void createDialog (String t, String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(homeview);
        LayoutInflater inflater = LayoutInflater.from(context);
        View addView = inflater.inflate(R.layout.dialog_avatar, null);
        builder.setTitle(t)
                .setView(addView)
                .setMessage(m)
                .setCancelable(true)
                .setPositiveButton("Conferma", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        closeDialog();
                        createPositiveDialog("Comando Inviato", "");
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private void createPositiveDialog(String t, String m) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(homeview);
        LayoutInflater inflater = LayoutInflater.from(context);
        View addView = inflater.inflate(R.layout.dialog_avatar, null);
        builder.setTitle(t)
                .setView(addView)
                .setMessage(m)
                .setCancelable(true)
                .setPositiveButton("ok", null);
        alert = builder.create();
        alert.show();
    }

    private void createErrorDialog(String t, String m) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(homeview);
        LayoutInflater inflater = LayoutInflater.from(context);
        View addView = inflater.inflate(R.layout.dialog_avatar, null);
        builder.setTitle(t)
                .setView(addView)
                .setMessage(m)
                .setCancelable(true)
                .setPositiveButton("riprova", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        homeview.invokeSpeech();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private void closeDialog(){
        alert.hide();
    }
}
