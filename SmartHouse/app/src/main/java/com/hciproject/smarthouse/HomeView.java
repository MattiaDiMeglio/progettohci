package com.hciproject.smarthouse;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

public class HomeView extends AppCompatActivity implements Observer {
    public TextView text;
    private String TAG = "HomeView";
    private FloatingActionMenu floatingActionMenu;//floating action menu per i dispositivi
    private static final int REQ_CODE_SPEECH_INPUT = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView avatar = (ImageView) findViewById(R.id.avatar);
        final TextView caffé = (TextView) findViewById(R.id.caffé);
        final TextView tempFrigo = (TextView) findViewById(R.id.tempFrigo);
        TextView contenutoFrigo = (TextView) findViewById(R.id.contenutoFrigo);
        final String contenutofrigorifero = contenutoFrigo.getText().toString();
        contenutoFrigo.setText(contenutofrigorifero + "?");

        caffé.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new VocalController(getApplicationContext(),caffé.getText().toString(),HomeView.this);
            }
        });
        tempFrigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new VocalController(getApplicationContext(),tempFrigo.getText().toString(),HomeView.this);
            }
        });
        contenutoFrigo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new VocalController(getApplicationContext(),contenutofrigorifero,HomeView.this);
            }
        });

        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invokeSpeech();
            }
        });

        floatingActionMenu = (FloatingActionMenu) findViewById(R.id.floatingActionMenu);
        new JsonController(getApplicationContext(), this);
    }

    public void invokeSpeech(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "In Ascolto!");
        startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    new VocalController(getApplicationContext(), result.get(0), HomeView.this);

                }
                break;
            }

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ottiene l'id dell'item selezionato
        int id = item.getItemId();


        if (id == R.id.settingsMenu) {
            Log.d(TAG, "premuto tasto impostazioni");
            //vai alle impostazioni
        }
        return (super.onOptionsItemSelected(item));
    }

    public void createFloatingMenu(){
        floatingActionMenu.setMenuButtonLabelText("Menu dispositivi connessi");
        final List<DispositiviModel> list = SingletonDispositiviModel.getInstance().getAllDisp();
        //crea i comandi nell'action button
        FloatingActionButton command[] = new FloatingActionButton[list.size() + 1];
        for (int i = 0; i < list.size(); i++) {
            FloatingActionButton programFab1 = new FloatingActionButton(this);
            programFab1.setButtonSize(FloatingActionButton.SIZE_MINI);
            programFab1.setLabelText(list.get(i).getNome());
            programFab1.setImageResource(R.drawable.ic_lightbulb);
            programFab1.setId(i);
            command[i] = programFab1;
            floatingActionMenu.addMenuButton(programFab1);
            command[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, list.get(view.getId()).getNome(), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    Log.d(TAG, list.get(view.getId()).getNome());
                    new AllViewController(getApplicationContext()).toPhisicalChannelView(view.getId());
                }
            });
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        createFloatingMenu();
    }
}
