package com.hciproject.smarthouse;

import android.content.Context;
import android.content.Intent;

/**
 * Created by andre on 12/07/2017.
 */

public class AllViewController {

    Context context;
    AllViewController(Context context){
        this.context = context;
    }

    public void toHomeView(){
        Intent homeView = new Intent(context, HomeView.class);
        homeView.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //flag aggiunto per migliorare la compatibilità con Xiaomi e Asus
        //fa partire l'activity con il nuovo intent
        context.startActivity(homeView);
    }

    public void toPhisicalChannelView(Integer index){
//imposta il nuovo intent per aprire una nuova activity
        Intent phisicalChannelView = new Intent(context, PhisicalChannel.class);
        //putExtra per passare l'index alla nuova activity
        phisicalChannelView.putExtra("index", index);
        phisicalChannelView.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //flag aggiunto per migliorare la compatibilità con Xiaomi e Asus
        //start della nuova activity
        context.startActivity(phisicalChannelView);
    }
}
