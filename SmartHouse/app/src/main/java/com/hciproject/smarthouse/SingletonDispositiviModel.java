package com.hciproject.smarthouse;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Created by andre on 13/07/2017.
 */

class SingletonDispositiviModel extends Observable {
    private static final SingletonDispositiviModel ourInstance = new SingletonDispositiviModel();

    static SingletonDispositiviModel getInstance() {
        return ourInstance;
    }

    private HomeView homeView;
    private List<DispositiviModel> dispVocal;
    private List<DispositiviModel> dispPhisical;
    private Object[] dispList;
    private List<DispositiviModel> allDisp;
    private SingletonDispositiviModel() {

    }

    public void setDispList (Object[] dispList){
        this.dispList = dispList;
        dispPhisical = (List<DispositiviModel>)dispList[0];
        dispVocal = (List<DispositiviModel>)dispList[1];
        allDisp = new ArrayList<DispositiviModel>();
        allDisp.addAll(dispPhisical);
        allDisp.addAll(dispVocal);
        setChanged();
        notifyObservers(dispList);
    }



    public List<DispositiviModel> getDispPhisical(){
        return dispPhisical;
    }

    public List<DispositiviModel> getDispVocal(){
        return dispVocal;
    }

    public List<DispositiviModel> getAllDisp(){
        return allDisp;
    }


    public Object [] getDispList(){
        return dispList;
    }

    public void observerAdd(HomeView homeView){
        clearObserver();
        this.homeView = homeView;
        addObserver(homeView);
    }

    private void clearObserver(){
        deleteObserver(homeView);
    }
}
