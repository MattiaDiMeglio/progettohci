package com.hciproject.museo;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Observable;

/**
 * Created by andre on 04/07/2017.
 */

public class SettingsModel extends Observable {

    private String TAG = "SettingsModel";
    private SharedPreferences sharedPreferences;
    private HomeView homeView;
    private OperaDescView operaDescView;
    private ListaOpereView listaOpereView;
    private static final String PREFS_NAME = "MyPrefsFile";//nome da dare alle sharedPreferences

    //costruttore base, utilizzato nel caso serva fare solo get
    SettingsModel(Context context){
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
    }
    //costruttore da ListOperaView
    SettingsModel(Context context, ListaOpereView listaOpereView){
        this.listaOpereView = listaOpereView;
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        addObserver(listaOpereView);
    }
    //costruttore da HomeView
    SettingsModel(Context ctx, HomeView home){
        homeView = home;
        sharedPreferences = ctx.getSharedPreferences(PREFS_NAME, 0);
        addObserver(homeView);
    }
    //costruttore da OpereDescView
    SettingsModel (Context context, OperaDescView operaDescView){
        this.operaDescView = operaDescView;
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
        addObserver(operaDescView);
    }
    //imposta la dimensione del testo
    public void setTextZoom(int textZoom){
        String prefZoom = "prefZoom";
        sharedPreferences.edit().putInt(prefZoom,textZoom).apply();
        setChanged();
        Integer size = textZoom;
        notifyObservers(size);
    }

    //imposta le preferenze per l'autoplay
    public void setAudio (boolean audio){
        String prefAudio = "prefAudio";
        sharedPreferences.edit().putBoolean(prefAudio,audio).apply();
        setChanged();
        notifyObservers(audio);
    }

    //imposta le preferenze per la lingua
    public void setLanguage (String language){
        String prefLang = "prefLang";
        sharedPreferences.edit().putString(prefLang,language).apply();
        setChanged();
        notifyObservers(sharedPreferences.getString(prefLang, "en"));
    }

    //imposta il nome del json da andare a leggere
    public void setJson(String json){
        String prefJson = "prefJson";
        sharedPreferences.edit().putString(prefJson, json).apply();
    }

    //restituisce il nome del json da leggere
    public String getJson(){
        String prefJson = "prefJson";
        String prova = sharedPreferences.getString(prefJson,"template.json");
        return prova;
    }

    //restituisce la lingua impostata
    public String getLanguage(){
        String prefLang = "prefLang";
        return sharedPreferences.getString(prefLang, "en");
    }

    //restituisce la dimensione del testo
    public int getTextZoom(){
        String prefZoom = "prefZoom";
        return sharedPreferences.getInt(prefZoom,0);
    }

    //restituisce le preferenze per l'autoplay
    public boolean getAudio (){
        String prefAudio = "prefAudio";
        return sharedPreferences.getBoolean(prefAudio, true);
    }

    //classe per il popolamento dello spinner
    public static class GetLanguages {
        private String textView;
        private Integer imageId;

        GetLanguages(String text, Integer imageId){
            this.textView=text;
            this.imageId=imageId;
        }

        public String getTextView(){
            return textView;
        }

        public Integer getImageId (){
            return imageId;
        }
    }
}
