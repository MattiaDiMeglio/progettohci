package com.hciproject.museo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by andre on 04/07/2017.
 */
//adapter per la creazione della struttura della lista di opere
public class ListaOpereAdapter extends BaseAdapter{

    /*Questa classe serve per compilare le singole righe della ListView contenuta in ListaOpereView per poi unirle insieme
    * sfruttando la risorsa xml single_row_list.xml ed il suo contenuto */
    private List<OpereModel> articles=null;
    private Context context=null;
    private int size;
    public ListaOpereAdapter(Context context, List<OpereModel> articles, int size)
    {
        this.articles=articles;
        this.context=context;
        this.size = size;
    }

    //restituisce il numero di elementi della lista
    public int getCount()
    {
        return articles.size();
    }
    //restituisce l'elemento alla posizione passata
    public Object getItem(int position)
    {
        return articles.get(position);
    }
    //restituisce l'id dell'elemento alla posizione passata
    public long getItemId(int position)
    {
        return getItem(position).hashCode();
    }

    //questa funzioneimposta i singoli campi della riga da aggiungere nella ListView
    public View getView(int position, View v, ViewGroup vg)
    {
        if (v==null)
        {
            v= LayoutInflater.from(context).inflate(R.layout.single_row_list, null);
        }
        //recupero dell'opera
        OpereModel ai=(OpereModel) getItem(position);
        //Dichiarazione dell'imageView
        ImageView imageView = (ImageView) v.findViewById(R.id.list_image);
        //Dichiarazione della textView
        TextView txt=(TextView) v.findViewById(R.id.artista);
        //settaggio dimensione del testo
        txt.setTextSize(10 + size);
        //valorizzazione della textview artista
        txt.setText(ai.getAutore());
        //dichiarazione textview
        txt=(TextView) v.findViewById(R.id.titolo_opera);
        //settaggio dimensione del testo
        txt.setTextSize(15 + size);
        //valorizzazione della textview titolo
        txt.setText(ai.getTitolo());
        //valorizzazione della imageView con l'anteprima dell'opera
        imageView.setImageDrawable(ai.getImage());

        return v;
    }
}
