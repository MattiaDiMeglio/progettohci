package com.hciproject.museo;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by andre on 04/07/2017.
 */

/* Questa classe fa parte del controller legato a ListaOpereView e OperaDescView*/

public class JsonController {

    private String TAG = "JsonController";//tag per il log
    //variabile per salvataggio del contesto
    private Context context;
    //nome del Jsonfile da leggere
    private String jSonFile;
    //dichiarazione della view che verrà successivamente inizializzata tramite costruttore
    private OperaDescView operaDescView;

    //costruttore chiamato da listaOpereView, per recuperare la lista opere
    JsonController(Context ctx, ListaOpereView listaOpereView, String jSonFile) {
        context = ctx; //inizializzazione per il contesto utilizzato nell'istanza attuale
        this.jSonFile = jSonFile;//assegnazione del file da cui leggere
        new GetListaView().execute(listaOpereView); // esecuzione dell'AsyncTask
    }

    JsonController(Context ctx, OperaDescView operaDescView, String jSonFile, Integer index) {
        context = ctx; //inizializzazione per il contesto utilizzato nell'istanza seguente
        this.jSonFile = jSonFile;//assegnazione del file da cui leggere
        this.operaDescView = operaDescView;//assegnazione della View a cui fare riferimento
        new GetDescOpera().execute(index); // esecuzione dell'AsyncTask
    }

    //Questo AsyncTask si occupa di creare il necessario da visualizzae nella view ListaOpereView
    private class GetListaView extends AsyncTask<ListaOpereView, Integer, Object[]> {


        ListaOpereView listaOpereView; // dichiarazione della view che varrà passata, alla fine dell'esecuzione, al costruttore del model
        Boolean connection = true; //flag per la connessione ad internet

        @Override
        protected Object[] doInBackground(ListaOpereView... objects) {
            listaOpereView = objects[0]; //assegnazione della view passata per parametro
            //verifica della connessione ad internet
            try {
                InetAddress ipAddr = InetAddress.getByName("www.google.com");
            } catch (Exception e) {
                connection = false;
                this.cancel(true);
            }
            List<OpereModel> opereArray = null; // lista di oggetti OpereModel che verrà restituita alla funzione OnPostExecute
            OpereModel tmp; //Oggetto temporaneo che verrà aggiunto di volta in volta ad opereArray

            //Dichiarazione delle variabili per la copia del contenuto del file Json
            String jsonStr;//stringa ottenuta dal input stream
            String temaSala = null;//stringa che conterrà il tema della sala
            InputStream is;//input stream per leggere dal file json
            int size;//size del buffer
            byte[] buffer;//buffer
            JSONObject jsonObj;//Jsonboject che conterrà il contenuto del file json, utile per il parsing delle singole parti
            Object ris[] = new Object[2]; //object che verrà ritornato dal metodo

            updateLanguage(listaOpereView);
            try {
                //copia del contenuto del file Json nella variabile String jsonStr
                is = context.getAssets().open(jSonFile);
                size = is.available();
                buffer = new byte[size];
                is.read(buffer);
                is.close();

                jsonStr = new String(buffer, "UTF-8");
                jsonObj = new JSONObject(jsonStr); //assegnazione della stringa ad un oggetto JSONObject

                // prelievo delle informazioni (tema della sala) dal json fino a raggiungere  il'array delle opere
                JSONObject opere = jsonObj.getJSONObject(listaOpereView.getResources().getString(R.string.language));
                opere = opere.getJSONObject("sala");
                temaSala = opere.getString("temaSala");
                JSONArray allOpere = opere.getJSONArray("opere");

                //allocazione della lista che verrà poi ritornata alla fine dell'esecuzione della funzione
                opereArray = new ArrayList<OpereModel>(allOpere.length());

                // scorrimento di tutto il json collezionando oggetti OpereModel appositamente riempiti
                for (int i = 0; i < allOpere.length(); i++) {

                    //tmp e' un oggetto OpereModel temporaneo che verrà poi aggiunto alla lista opereArray
                    tmp = new OpereModel(listaOpereView);
                    JSONObject c = allOpere.getJSONObject(i);

                    String titolo = c.getString("titolo");
                    tmp.setTitolo(titolo);

                    String autore = c.getString("autore");
                    tmp.setAutore(autore);

                    //download dell'immagine d'anteprima relativa alla seguente opera
                    String imageUrl = c.getString("anteprima");
                    is = (InputStream) new URL(imageUrl).getContent();
                    Drawable d = Drawable.createFromStream(is, "src name");
                    tmp.setImage(d);

                    //aggiunta dell'oggetto tmp alla lista da restituire a fine esecuzione
                    opereArray.add(tmp);
                    //percentuale di progresso dell'asynctask
                    publishProgress((int) ((i / (float) allOpere.length()) * 100));

                }

            } catch (JSONException | IOException e) {
                e.printStackTrace();
                this.cancel(true);
            }

            ris[0] = opereArray;
            ris[1] = temaSala;
            return ris;

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            listaOpereView.setTitleDialog(values[0].toString() + "%");
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (connection) {
                listaOpereView.createDialog(context.getResources().getString(R.string.errorLoading));
            } else {
                listaOpereView.createDialog(context.getResources().getString(R.string.errorConnection));
            }
        }

        @Override
        protected void onPostExecute(Object[] aVoid) {
            if (aVoid == null) {

            } else {
                //viengono passati i dati ottenuti al model, con la view a cui fare riferimento
                new OpereModel(listaOpereView).sendModel(aVoid);
            }

        }
    }

    //Questo AsyncTask si occupa di creare il necessario da visualizzae nella view OperaDescView
    private class GetDescOpera extends AsyncTask<Integer, Integer, Object[]> {


        protected Object[] doInBackground(Integer... v) {

            List<OpereModel> opereArray = null; // lista di oggetti OpereModel che verrà restituita alla funzione OnPostExecute
            OpereModel tmp; //Oggetto temporaneo che verrà aggiunto di volta in volta ad opereArray

            //inizializzazione delle variabili per la copia del contenuto del file Json nella variabile String jsonStr
            String jsonStr;
            InputStream is;
            int size = 0;
            byte[] buffer = null;
            JSONObject jsonObj = null;
            Object ris[] = new Object[4];
            JSONArray arrayComandi;

            updateLanguage(operaDescView); //recupero delle impostazioni per la lingua

            try {

                //copia del contenuto del file Json nella variabile String jsonStr
                is = context.getAssets().open(jSonFile);
                size = is.available();
                buffer = new byte[size];
                is.read(buffer);
                is.close();

                jsonStr = new String(buffer, "UTF-8");
                jsonObj = new JSONObject(jsonStr); //assegnazione della stringa ad un oggetto JSONObject

                // prelievo delle informazioni dal json fino a raggiungere l'array delle opere
                JSONObject opere = jsonObj.getJSONObject(operaDescView.getResources().getString(R.string.language));
                opere = opere.getJSONObject("sala");
                JSONArray allOpere = opere.getJSONArray("opere");
                opereArray = new ArrayList<OpereModel>(allOpere.length());

                // scorrimento di tutto il json fino al raggiungimento dell'indice desiderato
                for (Integer i = 0; i < allOpere.length(); i++) {

                    //tmp e' un oggetto OpereModel temporaneo che verrà poi aggiunto alla lista opereArray
                    tmp = new OpereModel(operaDescView);
                    JSONObject c = allOpere.getJSONObject(i);

                    String titolo = c.getString("titolo");
                    tmp.setTitolo(titolo);

                    String autore = c.getString("autore");
                    tmp.setAutore(autore);

                    //download dell'immagine d'anteprima relativa alla seguente opera
                    String imageUrl = c.getString("anteprima");
                    is = (InputStream) new URL(imageUrl).getContent();
                    Drawable d = Drawable.createFromStream(is, "src name");
                    tmp.setImage(d);

                    //recupero dei comandi veloci per l'opera
                    arrayComandi = c.getJSONArray("comandi");
                    String[] arrayDomande = new String[arrayComandi.length()];
                    String[] arrayRisposte = new String[arrayComandi.length()];
                    for (int j = 0; j < arrayComandi.length(); j++) {
                        JSONObject comandi = arrayComandi.getJSONObject(j);
                        arrayDomande[j] = comandi.getString("comando");
                        arrayRisposte[j] = comandi.getString("risposta");
                    }
                    tmp.setArrayComandi(arrayDomande);
                    tmp.setArrayRisposte(arrayRisposte);

                    //aggiunta dell'oggetto tmp alla lista da restituire a fine esecuzione
                    opereArray.add(tmp);

                    if (i == v[0]) {
                        ris[0] = c.getString("titolo");
                        ris[1] = c.getString("descrizione");
                        ris[3] = c.getString("suono");
                    }
                    //percentuale di progresso dell'asynctask
                    publishProgress((int) ((i / (float) allOpere.length()) * 100));

                }

                ris[2] = opereArray;
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }

            return ris;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            operaDescView.setTitleDialog(values[0].toString() + "%");
        }

        @Override
        protected void onPostExecute(Object[] object) {
            //invio dei dati ottenuti al model, con la view a cui fare riferimento
            new OpereModel(operaDescView).sendDesc(object);
        }
    }

    // update viene chiamato all'avvio dell'app per caricare le preferenze per la lingua
    private void updateLanguage(OperaDescView operaDescView) {
        Locale locale = new Locale(new SettingsModel(context).getLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        operaDescView.getResources().updateConfiguration(config, operaDescView.getResources().getDisplayMetrics());
    }

    private void updateLanguage(ListaOpereView listaOpereView) {
        Locale locale = new Locale(new SettingsModel(context).getLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        listaOpereView.getResources().updateConfiguration(config, listaOpereView.getResources().getDisplayMetrics());
    }

    private boolean isOnline() {
        try {
            InetAddress ipAddr = InetAddress.getByName("www.google.com"); //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}



