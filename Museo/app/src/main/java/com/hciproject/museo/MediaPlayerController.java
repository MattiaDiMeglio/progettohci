package com.hciproject.museo;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.PowerManager;

/**
 * Created by matti on 07/07/2017.

 */

public class MediaPlayerController{
    //ogetto mediaplayer che verrà usato dal controller per gestire la riproduzione
    private MediaPlayer mediaPlayer;
    //contesto dell'applicazione
    private Context context;
    //id del suono contenuto in R.raw
    private Integer suono;
    //variabile che rappresenta in che posizione del file si trova il player(millisecondi)
    private int pos;

    //costruttore del mediaplayer,
    public MediaPlayerController (Context context, Integer suono, boolean impostazioni){
        this.context=context;//valorizza il contesto con quello di OperaDescView
        this.suono=suono;//setta il nome del suono da riprodurre
        new CreatePlayer().execute();//execute dell'asynctask per creare il player
        //se impostazioni è true, è abilitato l'autoplay e fa partire l'asynctask di riproduzione audio
        if (impostazioni) {
            new StartPlayer().execute();
        }
    }
    //chiama l'asynctask StartPlayer, che lancia la riproduzione in un thread separato
    public void play(){
        new StartPlayer().execute();
    }

    //mette in pausa la riproduzione, per poi poterla far ripartire dallo stesso punto
    public void pause(){
        mediaPlayer.pause();
    }

    //metodo per il fast forward, ottiene la posizione, vi aggiunge 10 secondi
    //e sposta il player alla nuova posizione
    public void fastForward(){
        pos= mediaPlayer.getCurrentPosition();
        pos += 10000;
        mediaPlayer.seekTo(pos);
    }

    //metodo per il rewind, ottiene la posizione, vi toglie 10 secondi
    //e sposta il player alla nuova posizione
    public void rewind(){
        pos= mediaPlayer.getCurrentPosition();
        pos -= 10000;
        mediaPlayer.seekTo(pos);
    }

    //metodo per il stop, chiude il player
    public void stop(){
        mediaPlayer.stop();
    }

    //asynctask per la creazione del player ed il caricamento del file da riprodurre
    private class CreatePlayer extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            //crea il player
            mediaPlayer= MediaPlayer.create(context, suono);
            //setta il tipo di stream
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            //permette di continuare la riproduzione con lo schermo spento
            mediaPlayer.setWakeMode(context, PowerManager.PARTIAL_WAKE_LOCK);
            return null;
        }
    }

    //asynctask per la riproduzione del file caricato precedentemente
    private class StartPlayer extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            mediaPlayer.start();
            return null;
        }
    }

}
