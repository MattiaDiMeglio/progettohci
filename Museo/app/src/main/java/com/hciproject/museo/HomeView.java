package com.hciproject.museo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;

import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

//Clsse View della schermata Home
public class HomeView extends AppCompatActivity implements Observer {
    //variabile necessaria per la richiesta dei permessi di modifica storage
    private static final int MY_PERMISSIONS_REQUEST__WRITE_STORAGE = 0;

    private String TAG = "HomeView"; //TAG per l'utilizzo di Log

    //inizializzazione delle variabili globali
    private SettingsController settingsController;
    //radiobutton per la selezione dell'autoplay
    private RadioButton audioButton;
    //spinner per la selezione della lingua
    private Spinner languageSpinner;
    //variabile salvata per poter inviare la view al model corrispondente
    private HomeView homeView;
    private Boolean flag; //flag per la gestione del radio button

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Controlla il valore della lingua corrente
        updateLanguage();
        //Aggiunta del titolo all'action bar
        getSupportActionBar().setTitle(R.string.app_name);
        setContentView(R.layout.activity_home_view);
        homeView = this;
        //inizializzo il model e il controller per la view HomeView
        settingsController = new SettingsController(getApplicationContext(), this);

        //Valorizzazione dei widget
        languageSpinner = (Spinner) findViewById(R.id.languageSpinner);
        //Valorizzazione audioButton
        audioButton = (RadioButton) findViewById(R.id.audioRadio);
        //Valorizzazione okButton
        Button okButton = (Button) findViewById(R.id.okButton);

        update(); //metodo per il caricamento delle preferenze all'avvio

        //radiobutton per la scelta dell'audioguida
        flag = audioButton.isChecked(); //flag per la gestione del radio button
        audioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAudioButton(); //metodo per la gestione del flag del radiobutton
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AllViewController(getApplicationContext()).toOpereView();
            }
        });

        Spinner sp=(Spinner)findViewById(R.id.languageSpinner);
        //valorizzazione dello spin adapter, necessario per creare il layout interno allo spinner
        SpinnerAdapter adapter=new SpinnerAdapter(this,
                R.layout.spinner_language_selector,R.id.textViewSpinner, new SettingsController(getApplicationContext(), this).getLanguages());
        //settaggio dell'adapter
        sp.setAdapter(adapter);
        //listener per la selezione di un'elemento dello spinner
            sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1){
                    //settaggio della lingua italiana nel caso venga selezionato il primo valore dello spinner
                    String languageToLoad  = "it"; // your language
                    Locale locale = new Locale(languageToLoad);//cambio del locale
                    Locale.setDefault(locale);//settaggio del nuovo locale come default
                    Configuration config = new Configuration();//oggetto utile per il cambio della configurazione dell'app
                    config.locale = locale;//aggiornamento del locale nella configurazione
                    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());//update della configurazione
                    new SettingsController(getApplicationContext(),homeView).chooseLang(languageToLoad);//cambio della lingua nell'app
                }
                if (i == 2){
                    //settaggio della lingua inglese nel caso venga selezionato il secondo valore dello spinner
                    String languageToLoad  = "en"; // your language
                    Locale locale = new Locale(languageToLoad);//cambio del locale
                    Locale.setDefault(locale);//settaggio del nuovo locale come default
                    Configuration config = new Configuration();//oggetto utile per il cambio della configurazione dell'app
                    config.locale = locale;//aggiornamento del locale nella configurazione
                    getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());//update della configurazione
                    new SettingsController(getApplicationContext(),homeView).chooseLang(languageToLoad);//cambio della lingua nell'app
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //richieste dei permessi
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST__WRITE_STORAGE);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST__WRITE_STORAGE);
        }

    }

    //metodo per la gestione del flag del radiobutton
    private void setAudioButton(){
        if(flag){
            Log.d(TAG, "disattivo");
            audioButton.setChecked(false);
            flag = false;
            settingsController.audioOnOff(false);
        }else{
            Log.d(TAG, "attivo");
            audioButton.setChecked(true);
            flag = true;
            settingsController.audioOnOff(true);
        }
    }

    //metodo autoinvocato ad ogni modifica delle impostazioni
    @Override
    public void update(Observable observable, Object o) {
        //se è stata modificata la lingua
        if (o instanceof String){
            updateLanguage();
            Intent refresh = new Intent(this, HomeView.class);
            startActivity(refresh);
            finish();
        //se sono state modificate le preferenze sull'autoplay
        }else{
            audioButton.setChecked(new SettingsModel(getApplicationContext(),this).getAudio());
        }
    }

    // update viene chiamato all'avvio dell'app per caricare le preferenze per l'audio
    private void update() {
        audioButton.setChecked(new SettingsModel(getApplicationContext(),this).getAudio());
    }

    // update viene chiamato all'avvio dell'app per caricare le preferenze per la lingua
    private void updateLanguage (){
        Locale locale = new Locale(new SettingsModel(getApplicationContext()).getLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
}
