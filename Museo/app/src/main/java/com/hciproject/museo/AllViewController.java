package com.hciproject.museo;

import android.content.Context;
import android.content.Intent;

/**
 * Created by andre on 04/07/2017.
 */

/* Questo Controller si occupa dello spostamento tra le varie view del MVC*/
public class AllViewController {
    //variabile utile per l'invio ricevere il contesto dalle view
    private Context context;
    //costruttore che va semplicemente a settare il contesto
    AllViewController (Context ctx){
        context = ctx;
    }

    //metodo per andare a ListaOpereView
    public void toOpereView (){
        //imposta il nuovo intent per aprire una nuova activity
        Intent listaOpere = new Intent(context, ListaOpereView.class);
        listaOpere.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //flag aggiunto per migliorare la compatibilità con Xiaomi e Asus
        //fa partire l'activity con il nuovo intent
        context.startActivity(listaOpere);
    }
    //metodo per andare a OpedeDescView, l'index è necessario per identificare a quale opera si vuole arrivare
    public void toOperaDescView(int index){
        //imposta il nuovo intent per aprire una nuova activity
        Intent operaDescView = new Intent(context, OperaDescView.class);
        //putExtra per passare l'index alla nuova activity
        operaDescView.putExtra("index", index);
        operaDescView.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //flag aggiunto per migliorare la compatibilità con Xiaomi e Asus
        //start della nuova activity
        context.startActivity(operaDescView);
    }

    public void toOperaCommandView(String operaName) {
        //imposta il nuovo intent per aprire una nuova activity
        Intent operaCommandView = new Intent(context, OperaCommandView.class);
        operaCommandView.putExtra("name", operaName);
        operaCommandView.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //flag aggiunto per migliorare la compatibilità con Xiaomi e Asus
        //start della nuova activity
        context.startActivity(operaCommandView);
    }
}

