package com.hciproject.museo;

import android.content.Context;
import android.content.res.Configuration;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by andre on 04/07/2017.
 */

public class SettingsController {
    private SettingsModel settingsModel;
    private Context context;

    //costruttore per l'homeview
    public SettingsController(Context context, HomeView homeView) {
        settingsModel = new SettingsModel(context, homeView);
        this.context = context;
    }

    //costruttore per OperaDescView
    public SettingsController(Context context, OperaDescView operaDescView) {
        settingsModel = new SettingsModel(context, operaDescView);
    }

    //costruttore per ListaOpereView
    public SettingsController(Context applicationContext, ListaOpereView listaOpereView) {
        settingsModel = new SettingsModel(applicationContext, listaOpereView);
    }

    //lista delle lingue disponibili (utile per popoolare lo spinner nella homeview
    public ArrayList<SettingsModel.GetLanguages> getLanguages (){
        updateLanguage();
        ArrayList<SettingsModel.GetLanguages> languagesList = new ArrayList<SettingsModel.GetLanguages>(3);//numero di lingue supportate +1
        languagesList.add(new SettingsModel.GetLanguages(context.getResources().getString(R.string.chooseLang), R.mipmap.ic_world));
        languagesList.add(new SettingsModel.GetLanguages("Itailano", R.mipmap.ic_italian_flag));
        languagesList.add(new SettingsModel.GetLanguages("English", R.mipmap.ic_english_flag));
        return languagesList;
    }

    //va a cambiare la lingua impostata nel settingsModel
    public void chooseLang(String lang){
        settingsModel.setLanguage(lang);
    }
    //va a cambiare le impostatzioni per l'autoplay nel settingsModel
    public void audioOnOff(boolean audio){
        settingsModel.setAudio(audio);
    }
    //va a cambiare le impostazioni per la dimensione del testo nel settingsModel
    public void textZoom (int size){
        settingsModel.setTextZoom(size);
    }

    // update viene chiamato all'avvio dell'app per caricare le preferenze per la lingua
    private void updateLanguage (){
        Locale locale = new Locale(settingsModel.getLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }
}
