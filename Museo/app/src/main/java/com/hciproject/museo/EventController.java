package com.hciproject.museo;

import android.app.Service;
import android.content.Intent;

import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by matti on 10/07/2017.
 */
//classe lasciata in posizione per rappresentare la classe che ci sarebbe in caso di implementazione della comunicazione
//col sistema informatizzato del museo. Non è stato implementato perché non utilizato
public class EventController extends Service {


    public class EventBinder extends Binder {
        EventController getService() {
            return EventController.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //new JsonController(context, listaOpereView, jSonFile);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
