package com.hciproject.museo;

import android.annotation.TargetApi;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

public class OperaDescView extends AppCompatActivity implements Observer, Serializable {

    //dichiarazioni delle variabili utili alla view
    private String TAG = "OperaDescView"; //tag per il log
    private List<OpereModel> opereModelList;//lista delle opere
    private WebView myWebView;//webView per mostrare la descrizione dell'opera
    private int size;//dimensione del testo
    private ListView listView;//listView
    private FloatingActionMenu floatingActionMenu;//floating action menu per i comandi rapidi
    private boolean plays = false;//dichiarazione ed inizializzazione del flag di autoplay
    private MediaPlayerController mediaPlayerController;//mediaPlayerController
    private int id;//id opera
    private ImageButton play;//image button per il play/pause
    private ImageButton fastForward;//image button per il ff
    private ImageButton rewind;//image button per il rewinf
    private boolean audioSettings;//setting per l'autoplay
    private Integer index;//indice opera della lista
    private OpereModel list;//utile al floating button per recuperare i comandi specifici dell'opera
    private int i;//indice per il floating button
    private AlertDialog alert;//alertDialog per il caricamento


    //richiaeste dal mediaPlayer
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateLanguage();//controllo della lingua impostata
        setContentView(R.layout.activity_opera_desc_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);//settaggio della toolbar come supportActionBar
        SettingsModel settingsModel = new SettingsModel(getApplicationContext(), this);//dichiarazione del settings model
        audioSettings = settingsModel.getAudio();

        //valorizzazione dell'imageButton play
        play = (ImageButton) findViewById(R.id.play);

        //valorizzazione dell'imageButton fastForward
        fastForward = (ImageButton) findViewById(R.id.ff);

        //valorizzazione dell'imageButton fastForward
        rewind = (ImageButton) findViewById(R.id.rew);

        //inizializzazioni  delle variabili utili alla view
        final ImageButton arrow = (ImageButton) findViewById(R.id.arrowUp);
        myWebView = (WebView) findViewById(R.id.webview);
        listView = (ListView) findViewById(R.id.listViewOperaDesc);
        //crea l'alert dialog
        createDialog();

        //prelievo dell'indice dell'elemento della listview precedentemente scelta
        index = null;
        //ottenimento dell'index dell'opera nella lista
        Bundle extras;
        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if (extras == null) {
                index = null;
            } else {
                index = extras.getInt("index");
            }
        }

        //listener per il comprtamento dello SlidingUpMenu
        final SlidingUpPanelLayout slidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.operaDescView);
        slidingUpPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                //animazione del pulsante freccia ad ogni cambio slide dello SlidingUpMenu
                float deg = arrow.getRotation() + 180F;
                arrow.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {

            }
        });

        try {
            //popolamento della lista nello slideup menu
            new ComunicationController(this.getApplicationContext(), this, index);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //metodo per il bottone a forma di freccia che fa alzare e abbassare lo SlidingUpMenu
        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED)
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                else
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

            }
        });

        //metodo per il passaggio da un'opera all'altra mediante la listView nello SlidingUpMenu
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //blocca il mediaplyaer
                mediaPlayerController.stop();
                //cambio view
                new AllViewController(getApplicationContext()).toOperaDescView(listView.getPositionForView(view));

            }
        });

        myWebView = (WebView) findViewById(R.id.webview); //assegnazione della webView dove verranno caricate le informazioni sull'opera

        floatingActionMenu = (FloatingActionMenu) findViewById(R.id.menu);

        update(); //metodo utile per il caricamento delle preferenze
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //blocca l'audio se si torna alla lista opere
        mediaPlayerController.stop();

    }

    //popolamento della lista
    private void popLista() {
        listView.setAdapter(new ListaOpereAdapter(getApplicationContext(), opereModelList, size));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    //listener per gli elementi del floating button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // ottiene l'id dell'item selezionato
        int id = item.getItemId();


        if (id == R.id.textSizeMenu) {
            final OperaDescView operaDescView = this;//setta operaDescView
            final View addView = getLayoutInflater().inflate(R.layout.slider_menu, null);//aggiunge l'inflater per per lo slider
            //imposta e rende visibile l'alertDialog con lo slider per la dimensione del testo
            final AlertDialog alert = new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.textDimension) + " - " + size).setView(addView).show();
            SeekBar seekBar = (SeekBar) addView.findViewById(R.id.slider);

            seekBar.setProgress(size);//setta il valore iniziale dello slider con il valore attuale di size
            seekBar.setMax(10);//mette 10 come valore massimo per lo slider
            //listener per il cambio di valore nello slider
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    size = i;//setta il nuovo size
                    alert.setTitle(getResources().getString(R.string.textDimension) + " - " + size);//cabia il titolo dell'allertDialog
                    new SettingsController(getApplicationContext(), operaDescView).textZoom(size);//cambia la dimensione nell setting model
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
        return (super.onOptionsItemSelected(item));
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof Integer) { //nel caso in cui risulti vero, allora si tratta dello zoom della pagina HTML
            WebSettings settings = myWebView.getSettings();
            settings.setDefaultFontSize(16 + size);
            popLista();
        } else { //nel caso il primo controllo non fosse positivo, si procede con il popolamento della view
            myWebView.setWebViewClient(new WebViewClient());
            Object objects[] = (Object[]) o;

            getSupportActionBar().setTitle(objects[0].toString());
            myWebView.loadUrl(objects[1].toString()); //caricamento della pagina HTML bella webView

            opereModelList = (List<OpereModel>) objects[2];
            String suono = objects[3].toString();
            setMediaPlayerController(suono);
            popLista(); //popolamento della listview
            createCommandMenu();
            closeDialog();
        }
    }

    @Override
    protected void onRestart() {
        updateSize();//setta la dimensione del testo al restart
        super.onRestart();
    }

    //metodo utile per il caricamento delle preferenze
    private void update() {
        size = new SettingsModel(getApplicationContext(), this).getTextZoom();
        WebSettings settings = myWebView.getSettings();
        settings.setDefaultFontSize(16 + size);

    }

    //metodo utile per il caricamento della dimensione del testo
    private void updateSize() {
        size = new SettingsModel(getApplicationContext()).getTextZoom(); //prelievo delle preferenze sulla dimensione del testo
        popLista();
        WebSettings settings = myWebView.getSettings();
        settings.setDefaultFontSize(16 + size);
    }

    // update viene chiamato all'avvio dell'app per caricare le preferenze per la lingua
    private void updateLanguage() {
        Locale locale = new Locale(new SettingsModel(getApplicationContext()).getLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }


    private void createCommandMenu() {
        updateLanguage();
        floatingActionMenu.setMenuButtonLabelText(getResources().getString(R.string.commands));
        list = opereModelList.get(index);//ottiene l'opera all'indice index
        //crea i comandi nell'action button
        FloatingActionButton command[] = new FloatingActionButton[list.getArrayComandi().length + 1];
        for (i = 0; i < list.getArrayComandi().length; i++) {
            FloatingActionButton programFab1 = new FloatingActionButton(this);
            programFab1.setButtonSize(FloatingActionButton.SIZE_MINI);
            programFab1.setLabelText(list.getArrayComandi()[i]);
            programFab1.setImageResource(R.drawable.ic_lightbulb);
            programFab1.setId(i);
            command[i] = programFab1;
            floatingActionMenu.addMenuButton(programFab1);
            command[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, list.getArrayRisposte()[view.getId()], Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    Log.d(TAG, list.getArrayRisposte()[view.getId()]);
                }
            });
        }
        //creazione del bottone per andare ai comandi avanzati
        FloatingActionButton programFab1 = new FloatingActionButton(this);
        programFab1.setButtonSize(FloatingActionButton.SIZE_MINI);
        programFab1.setLabelText(getResources().getString(R.string.advancedCommands));
        programFab1.setImageResource(R.drawable.ic_advanced_command);
        programFab1.setId(list.getArrayComandi().length);
        command[list.getArrayComandi().length] = programFab1;
        floatingActionMenu.addMenuButton(programFab1);
        command[list.getArrayComandi().length].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AllViewController(getApplicationContext()).toOperaCommandView(getSupportActionBar().getTitle().toString());
            }
        });

    }

    //metodo per il settaggio ed il controllo del mediaplayer
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void setMediaPlayerController(String suono) {
        //crea il mediaPlayerController
        mediaPlayerController = new MediaPlayerController(getApplicationContext(), getResources().getIdentifier(suono, "raw", getPackageName()), audioSettings);
        //se è stato impostato l'autoplay setta il flag a true e imposta l'icona dell'imageview a quella di pause
        if (audioSettings) {
            plays = true;
            id = getResources().getIdentifier("pause", "drawable", "com.hciproject.museo");
            play.setBackground(getDrawable(id));
        }
        //onClickListener
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //se l'audio non è in play lo fa partire, setta il flag a true e cambia l'icona dell'image button a "pause"
                if (!plays) {
                    mediaPlayerController.play();
                    plays = true;
                    id = getResources().getIdentifier("pause", "drawable", "com.hciproject.museo");
                    play.setBackground(getDrawable(id));
                    //se l'audio è in play, lo stoppa, mette il flag a false e setta l'icona a "play"
                } else {
                    mediaPlayerController.pause();
                    plays = false;
                    id = getResources().getIdentifier("ic_play_dark", "drawable", "com.hciproject.museo");
                    ;
                    play.setBackground(getDrawable(id));

                }
            }
        });

        fastForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //se l'audio è in riproduzione, chiama fastForward
                if (plays) {
                    mediaPlayerController.fastForward();
                }
            }
        });

        rewind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //se l'audio è in riproduzione, chiama rewind
                if (plays) {
                    mediaPlayerController.rewind();
                }
            }
        });
    }

    //creazione dell'allertdialog con la percentuale restituita dall'asynctask
    public void createDialog() {
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View addView = inflater.inflate(R.layout.caricamento, null);
        alert = new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.loading) + " ").setView(addView).show();
        alert.setCancelable(false);
    }

    public void setTitleDialog(String t) {
        alert.setTitle(getResources().getString(R.string.loading) + " " + t);

    }

    public void closeDialog() {
        alert.hide();
    }
}
