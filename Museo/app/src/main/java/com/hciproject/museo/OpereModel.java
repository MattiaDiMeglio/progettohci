package com.hciproject.museo;

import android.graphics.drawable.Drawable;

import java.util.Observable;

/**
 * Created by andre on 04/07/2017.
 */
//model per l'opera
public class OpereModel extends Observable{
    private String titolo;
    private String autore;
    private Drawable image;
    private String arrayComandi[];
    private String arrayRisposte[];

    //costruttore che aggiunge l'observer listaOpereView
    OpereModel(ListaOpereView listaOpereView){
        addObserver(listaOpereView);
    }

    //costruttore che aggiunge l'observer listaOpereView

    public OpereModel(OperaDescView operaDescView) {
        addObserver(operaDescView);
    }

    //setta l'immagine di anteprima
    public void setImage(Drawable t){
        image = t;
    }
    //setta il titolo dell'opera
    public void setTitolo(String t){
        titolo = t;
    }
    //setta l'autore
    public void setAutore(String t){
        autore = t;
    }
    //setta l'array dei comandi
    public void setArrayComandi(String array[]){
        int i;
        int dim = array.length;
        arrayComandi = new String[array.length];
        for(i=0; i<dim; i++){
            arrayComandi[i]=array[i];
        }
    }
    //setta l'array delle risposte
    public void setArrayRisposte(String array[]){
        int i;
        int dim = array.length;
        arrayRisposte = new String[array.length];
        for(i=0; i<dim; i++){
            arrayRisposte[i]=array[i];
        }
    }
    //restituisce il titolo dell'opera
    public String getTitolo(){
        return titolo;
    }
    //restituisce l'autore dell'opera
    public String getAutore(){
        return autore;
    }
    //restituisce l'immagine
    public Drawable getImage() {
        return image;
    }
    //restituisce l'array dei comandi
    public String[] getArrayComandi() {
        return arrayComandi;
    }
    //restituisce l'array delle risposte
    public String[] getArrayRisposte() {
        return arrayRisposte;
    }
    //notifica l'obeserver listaOpereView dei cambiamenti
    public void sendModel(Object[] sala){
        setChanged();
        notifyObservers(sala);
    }
    //notifica l'obeserver OpereDescView dei cambiamenti
    public void sendDesc(Object[] ob) {
        setChanged();
        notifyObservers(ob);
    }
}


