package com.hciproject.museo;

import android.content.Context;

import java.io.IOException;

/**
 * Created by matti on 06/07/2017.
 *
 * */
//classe utile per la comunicazione tra l'app e l'event controller (in questo caso non rappresenta quella
//che dovrebbe essere la sua vera implementazione, ma è usata (insieme ad EventController, per simulare il cambio di sala
public class ComunicationController {
    //contesto
    private Context context;

    //Costruttore per la prima valorizzazione dela lista
    ComunicationController(Context ctx, ListaOpereView listaOpereView) throws IOException {
        context=ctx;
        new JsonController(context, listaOpereView, getJsonFile());
    };
    //costruttore utile per la simulazione del cambio di sala
    ComunicationController(Context ctx, ListaOpereView listaOpereView, int b) throws IOException {
        context=ctx;
        if(b==1){
            setJSonFile("template1.json");
        }else  if(b==2){
            setJSonFile("template.json");
        } else if (b==3){
            setJSonFile("");

        }
        new JsonController(context, listaOpereView, getJsonFile());

    };

    //costruttore per la lista creata in opereDescView
    ComunicationController(Context ctx, OperaDescView operaDescView, Integer index) throws IOException {
        context=ctx;
        new JsonController(context, operaDescView, getJsonFile(), index);

    };

    //setta il nome del JsonFile da far leggere all'applicazione
    public void setJSonFile(String file){
        new SettingsModel(context).setJson(file);
    }
    //ritorna il nome del JsonFile da far leggere all'applicazione
    public String getJsonFile(){
        return new SettingsModel(context).getJson();
    }
}
