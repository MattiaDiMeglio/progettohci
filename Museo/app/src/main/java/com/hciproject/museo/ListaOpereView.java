package com.hciproject.museo;

import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SeekBar;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

public class ListaOpereView extends AppCompatActivity implements Observer{

    private String TAG = "ListaOperaView";
    private List<OpereModel> op;//lista di opere
    private ListView listView;//listview
    private String nomeSala;//variabile per il nome della sala
    private int size;//dimensione testo
    private ListaOpereView listaOpereView;//variabile per il salvataggio della listaOpereView
    private AlertDialog alert;//alertDialog per il caricamento
    private int b;//flag per la simulazione della scansione, intero che può andare da 1 a tre, per rappresentare due stanze diverse ed il caso di errore
    private int indiceRefresh=1; //indice per contare il numero di refresh fatti, utile per la simulazione delle stanze
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listaOpereView = this;//settaggio di listaOpereView
        updateLanguage();//controllo per l'ottenimento della lingua
        setContentView(R.layout.activity_lista_opere_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);//dichiarazione della toolbar
        setSupportActionBar(toolbar);//settaggio della toolbar come supportActionbar
        size = new SettingsModel(getApplicationContext()).getTextZoom(); //prelievo delle preferenze sulla dimensione del testo

        b = 1;
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);//dichiarazione del floating button
        //settaggio del listener per far partire la scansione della sala
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    createDialog();
                    scansione(b);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        listView = (ListView) findViewById(R.id.listView);//dichiarazione della listview

        //listener degli elementi della lista
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                new AllViewController(getApplicationContext()).toOperaDescView(listView.getPositionForView(view));

            }
        });

        //crea l'alert dialog
        createDialog();

        //prima scansione della sala
        try {
            scansione();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //metodo per la prima scansione della sala
    private void scansione() throws IOException {
        new ComunicationController(getApplicationContext(), this);

    }

    //metodo per il refresh della lista opere
    private void scansione(int bool) throws IOException {
        new ComunicationController(getApplicationContext(), this, b);
        if (indiceRefresh<3) {
            if (b==3){b=1;}
            if (b == 1) {
                b = 2;
                indiceRefresh++;
            } else if (b == 2) {
                b = 1;
                indiceRefresh++;
            }
        } else {
            b=3;
            indiceRefresh=1;
        }
    }



    //settaggio del nome sala
    private void setNomeSala(){
        getSupportActionBar().setTitle(nomeSala);
    }

    //popolamento della listview con le opere estratte dal json
    private void popLista(){
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(new ListaOpereAdapter(getApplicationContext(), op, size));
        closeDialog();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu); //caricamento degli elementi presenti nel menu della actionbar
        return true;
    }

    //questa funzione funge da onClickListener per i pulsanti del menu nella actionbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //verifica del tasto premuto con la relativa operazione
        if (id == R.id.textSizeMenu) {
            //dichiarazione del Dialog e dello slider che servirà per modificare le dimensioni del testo
            final View addView = getLayoutInflater().inflate(R.layout.slider_menu, null);
            final AlertDialog alert = new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.textDimension) + " - " + size).setView(addView).show();
            SeekBar seekBar = (SeekBar) addView.findViewById(R.id.slider);

            seekBar.setProgress(size); //impostalo slider nella posizione corrispondente a quella della preferenza nelle impostazioni
            seekBar.setMax(10); //impostazione del massimo valore che può assumere lo slider

            //Listner dello slider
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    /*ad ogni spostamento, lo slider invierà al settings controller, che a sua voltà comunicherà
                      con il SettingsModel, la dimensione del testo*/
                    size = i;
                    alert.setTitle(getResources().getString(R.string.textDimension) + " - " + size);
                    new SettingsController(getApplicationContext(), listaOpereView).textZoom(size); //invio della dimensione al SettingsController
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            return (super.onOptionsItemSelected(item));

        }

        return super.onOptionsItemSelected(item);
    }

    //update della dimensione del testo al restart
    @Override
    protected void onRestart() {
        updateSize();
        super.onRestart();
    }

    //Update dell'observer sull'operaModel
    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof Integer) { //nel caso in cui risulti vero, allora si tratta della dimensione de testo
            size = (int) o;
            popLista();
        }else { //nel caso il primo controllo non fosse positivo, si procede con il popolamento della view
            Object o1[] = (Object[]) o;
            op = (List<OpereModel>) o1[0];
            nomeSala = o1[1].toString();
            popLista(); //popolamento della lista
            setNomeSala(); //impostazione del titolo della sala nell'actionbar
        }
    }
    //update della dimensione del testo
    private void updateSize(){
        size = new SettingsModel(getApplicationContext()).getTextZoom(); //prelievo delle preferenze sulla dimensione del testo
        popLista();
    }
//creazione dell'allertdialog con la percentuale restituita dall'asynctask
    public void createDialog (){
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View addView = inflater.inflate(R.layout.caricamento, null);
        alert = new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.loading) + " ").setView(addView).show();
        alert.setCancelable(false);
    }
    //creazione dell'allertdialog di errore nel caso non venga trovato il file json
    public void createDialog (String t){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.error)
                .setMessage(t)
                .setCancelable(false)
                .setPositiveButton(R.string.refresh, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            scansione(1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void setTitleDialog(String t){
        alert.setTitle(getResources().getString(R.string.loading) + " " + t);

    }

    public void closeDialog(){
        alert.hide();
    }

    // update viene chiamato all'avvio dell'app per caricare le preferenze per la lingua
    private void updateLanguage (){
        Locale locale = new Locale(new SettingsModel(getApplicationContext()).getLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
}



