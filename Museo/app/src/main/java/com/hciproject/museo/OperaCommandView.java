package com.hciproject.museo;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;

import java.util.ArrayList;
import java.util.Locale;

public class OperaCommandView extends AppCompatActivity {

    private static final int REQ_CODE_SPEECH_INPUT = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        updateLanguage();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opera_command_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                        getResources().getString(R.string.vocalCommand));
                startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);

            }
        });
        //prelievo dell nome dell'opera precedentemente scelta
        Bundle extras;
        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if (!(extras == null)) {
                getSupportActionBar().setTitle(extras.getCharSequence("name"));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    createDialog(result.get(0));
                }
                break;
            }

        }
    }

    public void createDialog(String message) {
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View addView = inflater.inflate(R.layout.caricamento, null);
        AlertDialog alert = new AlertDialog.Builder(this).setTitle(message).setView(addView).show();
        alert.setCancelable(true);
    }

    // update viene chiamato all'avvio dell'app per caricare le preferenze per la lingua
    private void updateLanguage() {
        Locale locale = new Locale(new SettingsModel(getApplicationContext()).getLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

}
