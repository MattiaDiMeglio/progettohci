package com.hciproject.museo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by andre on 10/07/2017.
 */


//Questa classe si occupa di creare uno spinner personalilzzato in base al numero di lingue supportate
public class SpinnerAdapter extends ArrayAdapter<SettingsModel.GetLanguages> {
    private int groupid;
    private ArrayList<SettingsModel.GetLanguages> list;
    private LayoutInflater inflater;
    //creazione dello spinner
    public SpinnerAdapter(Activity context, int groupid, int id, ArrayList<SettingsModel.GetLanguages> list){

        super(context,id,list);
        this.list=list;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupid=groupid;


    }

    //valorizazione degli elementi dello spinner
    public View getView(int position, View convertView, ViewGroup parent ){
        View itemView=inflater.inflate(groupid,parent,false);
        ImageView imageView=(ImageView)itemView.findViewById(R.id.imageViewSpinner);
        imageView.setImageResource(list.get(position).getImageId());
        TextView textView=(TextView)itemView.findViewById(R.id.textViewSpinner);
        textView.setText(list.get(position).getTextView());
        return itemView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup
            parent){
        return getView(position,convertView,parent);

    }
}
